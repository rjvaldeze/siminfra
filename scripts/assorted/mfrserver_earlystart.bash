#!/bin/bash
# Nissan, NAR, NCL, Suzuki, Infinity, Chrysler, NLAC, Todos, Mercedes, RCO, Hertz, Isuzu, FCO
#for m in i-0ea669836a1b10936 i-0efda81514aa7a9b1 i-019cd2a9ebfcda6a9 i-0104e5ee91c698a10 i-012157cbeb702b511 i-0ff3bdceb64f286dd i-0515a521fcc4ccc9e i-0bac719db7e2fdf24 i-0df4d9a525e6e37bd i-08363e0fe6487664a i-00e4f1d41ac419fee i-03beb4cb2d6175b5f i-0cc6c7d7e2d94fce1
# 2020 05 06 Se elimina instanceid de fco
for m in i-0ea669836a1b10936 i-0efda81514aa7a9b1 i-019cd2a9ebfcda6a9 i-0104e5ee91c698a10 i-012157cbeb702b511 i-0ff3bdceb64f286dd i-0515a521fcc4ccc9e i-0bac719db7e2fdf24 i-0df4d9a525e6e37bd i-08363e0fe6487664a i-00e4f1d41ac419fee i-03beb4cb2d6175b5f
do
   aws ec2 --region=us-east-1 start-instances --instance-ids $m
done
