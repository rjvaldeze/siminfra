#!/bin/bash
BASEDIR=/simetrical
if [ $# -lt 1 ]
then
  echo "You have to provide the number of the vm to start"
  echo "eg $0 20 will start usuario20 virutal machine"
else 
  $BASEDIR/ctl_virtualmachine.bash start $1
fi
