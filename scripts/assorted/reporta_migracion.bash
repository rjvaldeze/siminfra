#!/bin/bash
NOW=$(date)
RYVERINTEGRATIONURL="https://simetrical.ryver.com/application/webhook/RtwdlJKPOzqGHgG"
# El comando produce una línea con los IPs y los nombres de los servidores
PRDSERVERS=$(sudo aws ec2 describe-instances --region us-east-1 --filter Name=instance-state-name,Values=running Name=tag:Group,Values=PrdServers --query "Reservations[].Instances[].[PublicIpAddress, PrivateIpAddress, Tags[?Key=='Name'].Value]" --output text)
# Cuenta los palabras
NOPRDSERVERS=$(echo $PRDSERVERS|/usr/bin/wc -w)
# Las divide entre dos, por nombre de servers e IP pública y privada
NOPRDSERVERS=$(($NOPRDSERVERS/3))
PRDSERVERS=$(echo $PRDSERVERS|xargs -n3 echo)

MONSERVERS=$(sudo aws ec2 describe-instances --region us-east-1 --filter Name=instance-state-name,Values=running Name=tag:Group,Values=MonServers --query "Reservations[].Instances[].[PublicIpAddress, Tags[?Key=='Name'].Value]" --output text)
NOMONSERVERS=$(echo $MONSERVERS|/usr/bin/wc -w)
NOMONSERVERS=$(($NOMONSERVERS/2))
MONSERVERS=$(echo $MONSERVERS|xargs -n2 echo)
/usr/bin/curl --silent -X "POST" $RYVERINTEGRATIONURL -H "Content-Type: text/plain; charset=utf-8" -d "$NOW **PrdServers corriendo en total: $NOPRDSERVERS**"
/usr/bin/curl --silent -X "POST" $RYVERINTEGRATIONURL -H "Content-Type: text/plain; charset=utf-8" -d "$PRDSERVERS"
/usr/bin/curl --silent -X "POST" $RYVERINTEGRATIONURL -H "Content-Type: text/plain; charset=utf-8" -d "$NOW **MonServers corriendo en total: $NOMONSERVERS**"
/usr/bin/curl --silent -X "POST" $RYVERINTEGRATIONURL -H "Content-Type: text/plain; charset=utf-8" -d "$MONSERVERS"
#echo "$NOW prd: $NOPRDSERVERS - mon: $NOMONSERVERS" >> output.log
