#!/bin/bash -x

# Require one argument, one of the following
# start
# stop
# ban
# unban
# Optional second argument: Ip for ban/unban


# Display usage information
function show_usage {
  echo "Usage: $0 action <ip>"
  echo "Where action start, stop, ban, unban"
  echo "and IP is optional passed to ban, unban"
  exit
}


# Send notification
function send_msg {
  ryver_group=c8QEnGwjIX5NuXl
  ryver_url="https://simetrical.ryver.com/application/webhook/$ryver_group"
  host="heimdall.simetrical.net"
  msg="$@"

  curl --silent -X POST --header "Content-Type: text/plain" --header "charset: utf-8" --data "$msg (at **$host**)" $ryver_url
  exit
}


# Check for script arguments
if [ $# -lt 1 ]
then
  show_usage
fi
if [ $# -gt 1 ]
then
  COUNTRY=$(curl --silent http://api.ipstack.com/$2?access_key=cc81213950c353e76c5fb869a1686ed6 | /usr/local/bin/jp "country_name")
 #COUNTRY=$(echo $COUNTRY | tr " " "+")
fi
# Take action depending on argument
if [ "$1" = 'start' ]
then
  msg='Fail2ban just started'
  send_msg $msg
elif [ "$1" = 'stop' ]
then
  msg='Fail2ban was stopped'
  send_msg $msg
elif [ "$1" = 'ban' ]
then
  msg=$([ "$2" != '' ] && echo "#BANned $2 from $COUNTRY" || echo 'Fail2ban banned an ip' )
  send_msg $msg
elif [ "$1" = 'unban' ]
then
  msg=$([ "$2" != '' ] && echo "#UNBANned $2 from $COUNTRY" || echo "Fail2ban just unbanned an ip" )
  send_msg $msg
else
  show_usage
fi
