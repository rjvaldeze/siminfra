#!/bin/bash 
INSTANCESFILE=/tmp/instances.txt
DNSRECORD=/tmp/dns-record.json.tmp
LOGFILE="/tmp/register_prdservers_$(date +%Y%m%d)"
DNSRECORDTEMPLATE=/simetrical/dns-record.json
ROUTE53ZONE=ZHJJP4OSL0D6H
aws ec2 describe-instances --region us-east-1 --filter Name=instance-state-name,Values=running Name=tag:Group,Values=PrdServers Name=tag:Version,Values=Migracion --query 'Reservations[].Instances[?!not_null(Tags[?Key==`DNSRegistered`].Value)]|[].[InstanceId, Tags[?Key==`Name`].Value|[0], PrivateIpAddress]' --output text > $INSTANCESFILE
if [ -s $INSTANCESFILE ]
then
    cat $INSTANCESFILE | while read -r INSTANCEID NAME PRIVATEIP
    do 
        echo "[ $(date +'%Y/%m/%d %T')] INSTANCEID: $INSTANCEID NAME: $NAME, IP: $PRIVATEIP" >> $LOGFILE 2>&1
        cp $DNSRECORDTEMPLATE $DNSRECORD
        /bin/sed -ie "s/NEWIPGOESHERE/$PRIVATEIP/" $DNSRECORD
        /bin/sed -ie "s/HOSTNAMEGOESHERE/$NAME.simetrical.internal/" $DNSRECORD
        aws route53 change-resource-record-sets --hosted-zone-id $ROUTE53ZONE --change-batch file://$DNSRECORD >> $LOGFILE 2>&1
        aws ec2 create-tags --resources $INSTANCEID --tags Key="DNSRegistered",Value="Yes" --region us-east-1>> $LOGFILE 2>&1
    done 
    rm $DNSRECORD
fi
rm $INSTANCESFILE
