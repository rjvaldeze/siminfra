#!/bin/bash
if [ $# -lt 1 ]
then
   echo "Se esperaba al menos un parametro"
   exit 1
else
   INSTANCIASENCENDIDAS=$(sudo aws ec2 describe-instances --region us-east-1 --filter Name=instance-state-name,Values=running Name=tag:Group,Values=UsrServer --query "Reservations[].Instances[].InstanceId" | tr '[]\",' ' ' | xargs echo)
   for instancia in $INSTANCIASENCENDIDAS
   do
       read -r SCHEDULE SUBDOMAIN <<<$(/usr/bin/mysql --skip-column-names --batch app_produccion -h database.simetrical.net -u ctlserver -P5123 -pS1m3tr1c@lDB2010 -e "select horario, subdominio from v_infra_usuarios_instancias where instancia='$instancia';")
       if [ $SCHEDULE = $1 ]
       then
          echo "Se apagaria la instancia $instancia"
          /simetrical/stop_virtualmachine.bash $SUBDOMAIN
       fi
   done
fi
