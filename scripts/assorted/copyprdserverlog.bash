#!/bin/bash
#
# Transfiere los archivos /var/log/SCApp/Scorecard/#.log a un directorio llamado como la instancia (prdserver#)
# # es el número de cliente

INSTANCIAS=`aws ec2 describe-instances --filters Name=tag:Group,Values=PrdServers Name=tag:Version,Values=Migracion Name=instance-state-name,Values=running --query "Reservations[].Instances[].Tags[?Key=='Name'].Value" --output text`
for i in $INSTANCIAS
do 
    if [ ! -d $i ]
    then
       mkdir $i
    else
       echo No se creará el directorio $i/
    fi
    CLIENTNO=${i#"PrdServer"}
    scp -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" $i.simetrical.internal:/var/log/SCApp/Scorecard/$CLIENTNO.log $i/
done
