#!/bin/bash
# Septiembre 2020, RJVE
# Resumen
# Sin argumentos, mueve el archivo del día en /mnt/efs/external/tsiminv/ a s3://inventory.backup/AÑO/MES/
# Con un argumento, mueve el archivo que se le pida a s3://inventory.backup/AÑO/MES/
# 
MONTH=$(date +%m)
YEAR=$(date +%Y)
if [ $# -ge 1 ]
then
  aws s3 mv $1 s3://inventory.backup/$YEAR/$MONTH/ --profile rim
  rem echo Last exit status $?
else
  DAY=$(date +%d)
  aws s3 mv /mnt/efs/external/tsiminv/originales/*$DAY.zip s3://inventory.backup/$YEAR/$MONTH/ --profile rim
  echo Last exit status $?
fi

