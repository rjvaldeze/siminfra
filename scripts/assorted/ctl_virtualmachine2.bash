#!/bin/bash
SUBDOMAIN=$2
OPERATION=$1
ANNOUNCERYVER=$3
echo "Will start VM with subdomain $SUBDOMAIN"
echo "Querying for the VMs instance-id"
read -r RYVERUSER INSTANCEID <<<$(/usr/bin/mysql --skip-column-names --batch app_produccion -h database.simetrical.net -u ctlserver -P5123 -pS1m3tr1c@lDB2010 -e "select usuario, instancia from v_infra_usuarios_instancias where  subdominio='$SUBDOMAIN';")
if [ -z $RYVERUSER -o -z $INSTANCEID ]
then
   echo "Something's off... got an empty response"
   exit 1
fi
if [ $OPERATION != 'start' -a $OPERATION != 'stop' ]
then
   echo $OPERATION is not a valid operation
   exit 1
fi
   echo "Got instance-id $INSTANCEID"
   if [ "$OPERATION" = "start" ]
   then
      MSG="**$RYVERUSER** Iniciando la maquina virtual **$SUBDOMAIN**"
   else
      MSG="**$RYVERUSER** Deteniendo la maquina virtual **$SUBDOMAIN**"
   fi
 if [[ -z "$ANNOUNCERYVER" ]]
   then
   curl -X "POST" "https://simetrical.ryver.com/application/webhook/fK4gWYY0snGFmUv" -H "Content-Type: text/plain; charset=utf-8" -d "$MSG" 
fi
   echo "Asking the server to $OPERATION the instance and update the DNS entry for $SUBDOMAIN.simetrical.net"
   URL="wget -q -O- \"aws.simetrical.net/ec2.php?instancia=$INSTANCEID&estado=$OPERATION&zona=Z23F0CASG008TG&subdominio=$SUBDOMAIN.simetrical.net\""
   echo "Will ask for $URL"
   OUTPUT=$(eval $URL)
   echo  Got $OUTPUT
   if [[ -z "$ANNOUNCERYVER" ]]
   then
   curl -X "POST" "https://simetrical.ryver.com/application/webhook/fK4gWYY0snGFmUv" -H "Content-Type: text/plain; charset=utf-8" -d "$OUTPUT"
   fi

