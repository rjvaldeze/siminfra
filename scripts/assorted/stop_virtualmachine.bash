#!/bin/bash
BASEDIR=/simetrical
if [ $# -lt 1 ]
then
  echo "You have to provide the number of the vm to stop"
  echo "eg $0 20 will stop usuario$1 virtual machine"
else
  $BASEDIR/ctl_virtualmachine.bash stop $1
fi
