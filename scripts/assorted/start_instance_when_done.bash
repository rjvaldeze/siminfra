#!/bin/bash
INSTANCE=$1
NAME=$2
PENDING=$(aws ec2 describe-snapshots --filters Name=status,Values=pending Name=tag:Name,Values=$NAME --region us-east-1 --output text)
echo Checking if snapshot with name $NAME is done
echo $PENDING
if [[ -z $PENDING ]]
then
#   echo "Will modify volume"
#   aws ec2 modify-volume --volume-id vol-02ea13770f3dca2f6 --size 20 --region us-east-1 
   STATE=$(aws ec2 describe-instances --instance-id $INSTANCE --query "Reservations[].Instances[].State.Name[]" --output text --region us-east-1)
   if [ "$STATE" = "stopped" ]
   then 
      aws ec2 start-instances --instance-id $INSTANCE --region us-east-1 | mail fmedina@simetrical.com
   fi
fi
