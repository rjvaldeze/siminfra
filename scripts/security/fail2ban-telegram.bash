#!/bin/bash

# Sends text messages using Telegram
# to alert webmaster of banning.

# Require one argument, one of the following
# start
# stop
# ban
# unban
# Optional second argument: Ip for ban/unban


# Display usage information
function show_usage {
  echo "Usage: $0 action <ip>"
  echo "Where action start, stop, ban, unban"
  echo "and IP is optional passed to ban, unban"
  exit
}


# Send notification
function send_msg {
  apiToken=1379544674:AAFaTt7nYSo2sXPv1iDm9M3bkPE1KWEzpjE
  chatId=277923083
  url="https://api.telegram.org/bot$apiToken/sendMessage"

  curl --silent -X POST $url -d chat_id=$chatId -d parse_mode="markdown" -d text="$1"
  exit
}


# Check for script arguments
if [ $# -lt 1 ]
then
  show_usage
fi
#if [ $# -gt 1 ]
#then
#  COUNTRY=$(curl --silent http://api.ipstack.com/$2?access_key=cc81213950c353e76c5fb869a1686ed6 | /usr/local/bin/jp "country_name")
#  COUNTRY=$(echo $COUNTRY | tr " " "+")
#fi
# Take action depending on argument
if [ "$1" = 'start' ]
then
  msg='Fail2ban+just+started'
  send_msg $msg
elif [ "$1" = 'stop' ]
then
  msg='Fail2ban+just+stoped'
  send_msg $msg
elif [ "$1" = 'ban' ]
then
  COUNTRY=$(curl --silent http://api.ipstack.com/$2?access_key=cc81213950c353e76c5fb869a1686ed6 | /usr/l    ocal/bin/jp "country_name")
  COUNTRY=$(echo $COUNTRY | tr " " "+")
  msg=$([ "$2" != '' ] && echo "Fail2ban+just+banned+$2+%28%2A$COUNTRY%2A%29" || echo 'Fail2ban+just+banned+an+ip' )
  send_msg $msg
elif [ "$1" = 'unban' ]
then
#  msg=$([ "$2" != '' ] && echo "Fail2ban+just+unbanned+$2+%28%2A$COUNTRY%2A%29" || echo "Fail2ban+just+unbanned+an+ip" )
#  send_msg $msg
else
  show_usage
fi
