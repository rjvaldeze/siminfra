#!/bin/bash
# 
# Dependencies
# This script parses the JSON output provided by ipstack with the jp JMESPath implementation
# https://github.com/jmespath/jp

# Synopsis
# This script looks for lines in the /var/log/fail2ban.log with the specified date (or current date 
# if no parameter is given) and the "Found" string
# The matching lines will have their IP extracted and this IP will be consulted for geolocation in
# ipstack.com. The ip and country_name fields will be extracted via jamespath
#
# Scriptor
# rvaldez@simetrical.com.mx

FAIL2BANLOG=/var/log/fail2ban.log
JP=/usr/local/bin/jp
if [ $# -lt 1 ]
then 
   DATE=$(date +"%Y-%m-%d")
else
   DATE=$1
fi
echo Checking $FAIL2BANLOG for \"Found\" entries on date $DATE
IPS=$(sudo grep $DATE $FAIL2BANLOG | grep Found | awk '{print $8}')
NUMBEROFPROBES=$(echo $IPS | wc -w)
echo $NUMBEROFPROBES probes so far on $DATE
echo Detail follows
for i in $IPS
do
   curl --silent http://api.ipstack.com/$i?access_key=cc81213950c353e76c5fb869a1686ed6 | $JP "[ ip, country_name ]"
done
