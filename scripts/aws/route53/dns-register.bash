#!/bin/bash
INSTANCEID=$(/usr/bin/curl --silent http://169.254.169.254/latest/meta-data/instance-id)
echo My instance id is $INSTANCEID
EXTERNALIP=$(/usr/bin/aws ec2 describe-instances --instance-id $INSTANCEID --region us-east-1 --query "Reservations[].Instances[].PublicIpAddress" --output text)
if [ $? -ne 0 ] || [ -z $EXTERNALIP ]
then
    echo Could not get external ip: $EXTERNALIP, exit status: $?
    exit 1
fi

echo My external IP is $EXTERNALIP
NICKNAME=$(cat /simetrical/nickname)
echo My nickname is $NICKNAME
TMPFILE=/tmp/tmp-record.json
cp /simetrical/dns-record.json $TMPFILE
# Registers to SIMETRICAL.NET
/bin/sed -ie "s/NEWIPGOESHERE/$EXTERNALIP.simetrical.net/" $TMPFILE
/bin/sed -ie "s/HOSTNAMEGOESHERE/$NICKNAME/" $TMPFILE
echo Crafted $TMPFILE
cat $TMPFILE
# Registers to SIMETRICAL.NET
/usr/bin/aws route53 change-resource-record-sets --hosted-zone-id Z23F0CASG008TG --change-batch file://$TMPFILE
echo Deleting $TMPFILE
rm $TMPFILE
