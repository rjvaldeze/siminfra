import boto3

ec2 = boto3.client('ec2')

def getKey(dictionary, key):
    try:
        return dictionary[key]
    except KeyError:
        return None


def lambda_handler(event, context):
    #filters = [ {"Name": "tag:Group", "Values": [ "SimServer" ]} ]
    filters = []
    instances = ec2.describe_instances( Filters=filters )
    for i in range(len(instances['Reservations'])):
        instance = instances['Reservations'][i]['Instances'][0]
        print(instance["Placement"]["AvailabilityZone"], " ", getKey(instance,'Platform'), " ", getKey(instance,'Tags'))
        print("="*20)
        

    



lambda_handler(None,None)