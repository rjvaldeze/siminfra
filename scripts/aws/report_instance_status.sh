#!/bin/bash
# This script requires the configuration of the AWS with a policy of EC2 Read Only

AWSCLI=aws
RETURNSTATUS=0
RETURNSTRING=""
AWS_ACCESS_KEY_ID=AKIATLK7BNWAGC6UVBP6
AWS_SECRET_ACCESS_KEY=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
AWS_DEFAULT_REGION=us-east-1


if [ $# -ne 1 ]
then
    echo "I need an Instance Id to check on"
    exit 255
fi
STATUSES=$($AWSCLI ec2 describe-instance-status --instance-ids $1 \
             --query "InstanceStatuses[].{ Power: InstanceState.Name, \
             System: InstanceStatus.Details[].Status|[0], \
             Instance: SystemStatus.Details[].Status|[0] }|[0]" \
           )
if [ "$STATUSES" = "null" ]
then
    RETURNSTRING="OK - The instance is stopped, no checks performed"
else
    #echo "This is what I got: $STATUSES"
    POWERSTATE=$(echo $STATUSES | jq '.Power')
    NETWORKREACHABILITY=$(echo $STATUSES | jq '.System')
    INSTANCESTATUS=$(echo $STATUSES | jq '.Instance')
    #echo "Powerstate: $POWERSTATE, NetworkReachability: $NETWORKREACHABILITY, InstanceStatus: $INSTANCESTATUS"
    if [ $POWERSTATE = '"running"' ] # Observe that the double quotes are part of the value we've got from the echo | jq commands
    then
        #echo "The instance is running"
        if [ $NETWORKREACHABILITY = '"impaired"' ]
        then
            RETURNSTRING="System Reachability Test Failed. The state of the instance is IMPAIRED"
            RETURNSTATUS = 2
        fi
        if [ $INSTANCESTATUS = '"impaired"' ]
        then
            RETURNSTRING="$RETURNSTRING\nInstance Reachability Test Failed. The state of the instance is IMPAIRED"
            RETURNSTATUS=2
        fi
        if  [ $INSTANCESTATUS != '"impaired"' -a $NETWORKREACHABILITY != '"impaired"' ] 
        then
            RETURNSTRING="OK - Instance seems to have no problems"
        fi
    fi
fi
echo "$RETURNSTRING"
exit $RETURNSTATUS