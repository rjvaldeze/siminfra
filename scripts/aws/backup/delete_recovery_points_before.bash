#!/bin/bash
# Dependencies
# The awscli should be isntalled, configured and in the path
# https://github.com/aws/aws-cli
#
# Synopsis
# The default retention for recovery points on AWS is too much
# for our needs. This script deletes recovery points before a date
# so our costs go down. To check the backup dates:
# aws backup list-recovery-points-by-backup-vault --backup-vault Default --query "RecoveryPoints[].CreationDate" | uniq
# so a default 
# This script expects a string with the format YYYYMMDD
# ./delete_recovery_points_before.bash 20200702
# to delete the backup points before that date
# The recovery points are expected to be in the Default vault
# The awscli is expected to be configured and in the path
#
# Scriptor
# rvaldez@simetrical.com.mx

if [ $# -lt 1 ]
then
   echo No date provided. Exiting
   exit 255
fi
echo Will search for recovery points before $1
TIMESTAMP=$(date -d "$1" '+%s')
echo Timestamp is $TIMESTAMP
ARNS=$(aws backup list-recovery-points-by-backup-vault --backup-vault-name Default --by-created-before $TIMESTAMP --query "RecoveryPoints[].RecoveryPointArn" --output text)
if [ ! -z "$ARNS" ]
then
   for a in $ARNS
   do
      echo "Will delete $a"
      aws backup delete-recovery-point --backup-vault-name Default --recovery-point-arn $a
   done
fi
