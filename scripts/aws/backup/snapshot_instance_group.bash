#!/bin/bash

#
# Synopsis: Creates snapshots for volumes of instances when it is run and the server are stopped
# Finds out the volumes attached to an instance with a Group tag of UsrServer 
# 
# TODO: Parameterize to allow for Group name or other conditions like instance state
# 
# Ruben J Valdez Escobedo
# 2020 07 29
#

DATE=$(date +'%Y%m%d')
ARRAY=($(aws ec2 describe-instances --filters Name=tag:Group,Values=UsrServer Name=instance-state-name,Values=stopped --query "Reservations[].Instances[].[ Tags[?Key=='Name'].Value, BlockDeviceMappings[].Ebs.VolumeId ]" --output text))
for (( i=0; i<${#ARRAY[@]}; i=i+2 ))
do
    instance=${ARRAY[i]} 
    volume=${ARRAY[i+1]}
    echo "Askinf for a snapshot for $volume attached to $instance"
    aws ec2 create-snapshot --description "$instance-$DATE" --volume-id $volume --tag-specifications "ResourceType=snapshot,Tags=[{Key=Group,Value=UsrServer},{Key=Name,Value=$instance},{Key=Date,Value=$DATE}]"
done