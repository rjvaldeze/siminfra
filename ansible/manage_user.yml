---
#
# Añade o elimina un usuario, su llave y un archivo en sudoers en el sistema
# Esto permite:
# 1. Que el usuario puede iniciar su sesión sin password, teniendo su llave privada
# 2. Que el usuario pueda ejecutar cualquier comando como superusuario (sudo)
# 
# Ejemplo de invocación
# ansible-playbook -e "user_vars=servers/sim1.tech/users/psanchez.yaml" manage_user.yaml
# 
# Febrero/2021, RJVE
#
- hosts: "{{ host }}"
  vars_files: "{{ user_vars }}"
  tasks:
  - name: Create or delete user {{ user }}
    user:
        name: "{{ user }}" 
        generate_ssh_key: "{{generate_key|default(false)}}"
        shell: "{{ shell | default('/bin/bash') }}"
        groups: "{{user_groups|default()}}"
        state: "{{state|default(present)}}"
        remove: yes
    become: yes
  - name: Create the .ssh directory
    file:
        path: /home/{{ user }}/.ssh
        group: "{{ user }}"
        state: directory
        mode: '0700'
        owner: "{{ user }}"
    become: yes
    when: 
      - state is defined
      - state == "present"
  - name: Transfer the key for {{ user }}
    authorized_key:
        user: "{{ user }}" 
        state: present
        key: "{{ lookup('file', '{{ user_key }}') }}"
    become: yes
    when: 
      - state is defined
      - state == "present"
  - name: Allow the user to use sudo
    template:
        dest: /etc/sudoers.d/{{ user }} 
        src: sudoer.j2          
        backup: yes
    when: 
      - is_sudo_allowed 
      - state is defined
      - state == "present"
    become: yes
  - name: Remove the sudoers file if present
    file:
        path: /etc/sudoers.d/{{ user }} 
        state: absent
    when: 
      - is_sudo_allowed 
      - state == "absent"
    become: yes
  - name: Transfer the bashrc for {{ user }}
    copy:
        dest: /home/{{ user }}/.bashrc 
        src: bashrc
        group: "{{ user }}" 
        mode: '0644'
        owner: "{{ user }}"
        backup: yes
    become: yes
    when: 
      - state is defined
      - state == "present"
  - name: Transfer the tmux.conf for {{ user }}
    copy:
        dest: /home/{{ user }}/.tmux.conf 
        src: tmux.conf
        group: "{{ user }}" 
        mode: '0644'
        owner: "{{ user }}"
        backup: yes
    become: yes
    when: 
      - state is defined
      - state == "present"
  - name: Transfer the vimrc for {{ user }}
    copy:
        dest: /home/{{ user }}/.vimrc
        src: vimrc
        group: "{{ user }}" 
        mode: '0644'
        owner: "{{ user }}"
        backup: yes
    become: yes
    when: 
      - state is defined
      - state == "present"
