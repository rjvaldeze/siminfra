---
# Creates a UsrServer with the data specified in the usrserver_vars file
# Main steps taken:
# 1. Create the instance
# 2. Add a RR in the internal DNS
# 3. Add a RR in the external DNS is needed
# 4. Get the password for the Administrator
#
# The latter steps rely on the use of WinRM and this is not routed outside the VPC
# so the playbook should be run on the VPC network
# 
# ansible-playbook --extra-vars "aws_creds=simetrical_creds.yml usrserver=servers/usrserver/usuario80.yml" --ask-vault create_usrserver.yml
#
- hosts:
    localhost
  vars_files:
    - "{{aws_creds}}"
    - "{{usrserver}}"
  tasks:
  - name: Get info about the subnet
    ec2_vpc_subnet_info:
      aws_secret_key: "{{aws_secret_access_key}}"
      aws_access_key: "{{aws_access_key}}"
      subnet_ids: "{{vpc_subnet_id}}"
    register: subnet_data
  - debug:
      msg: "{{subnet_data}}"
  - name: Start an instance for "{{instance_name}}"
    ec2_instance:
      aws_secret_key: "{{aws_secret_access_key}}"
      aws_access_key: "{{aws_access_key}}"
      image_id: "{{instance_ami}}"
      instance_type: "{{instance_type}}"
      cpu_credit_specification: unlimited
      key_name: "{{key_name}}"
      network:
        delete_on_termination: yes
        assign_public_ip: "{{wants_public_ip}}"
      name: "{{instance_name}}"
      termination_protection: yes
      security_groups: "{{security_groups}}"
      vpc_subnet_id: "{{vpc_subnet_id}}"
      tags: "{{instance_tags}}"
      state: started
    register: instancedata
  - debug:
      var: instancedata
  - set_fact:
      host_name: "usuario{{instance_name[9:]}}"
  - name: Add a Resource Record for the instance {{instancedata.instances[0].instanceid}} in the internal dns zone
    route53: 
      aws_secret_key: "{{aws_secret_access_key}}"
      aws_access_key: "{{aws_access_key}}"
      hosted_zone_id: "{{internal_dns_zone_id}}"
      record: "{{host_name}}.{{internal_domain_name}}" 
      state: present
      type: CNAME
      ttl: 60
      overwrite: yes
      value: "{{instancedata.instances[0].private_dns_name}}"
  - name: Add a Resource Record for the instance {{instancedata.instances[0].instanceid}} in the public dns zone
    route53: 
      aws_secret_key: "{{aws_secret_access_key}}"
      aws_access_key: "{{aws_access_key}}"
      hosted_zone_id: "{{public_dns_zone_id}}"
      record: "{{host_name}}.{{public_domain_name}}" 
      state: present
      type: CNAME
      ttl: 60
      overwrite: yes
      value: "{{instancedata.instances[0].public_dns_name}}"
    when: wants_public_dns
  - name: Get the password for the Administrator
    ec2_win_password:
        instance_id: "{{instancedata.instances[0].instance_id}}"
        key_file: "{{key_file}}"
        region: "{{ instancedata.instances[0].placement.availability_zone[:-1]}}"
    register: windowspassword
  - name: Show the password
    debug:
      msg: "{{windowspassword}}"
  - name: set the variable hostName
    set_fact:
      hostName: "{{host_name}}.{{internal_domain_name}}"
  - name: Add the host to the in memory inventory
    add_host: 
      hostname: "{{hostName}}"
      ansible_hostname: "{{hostName}}"
      ansible_user: "Administrator"
      ansible_password: "{{windowspassword.win_password}}"
      ansible_connection: winrm
      ansible_winrm_transport: basic
      ansible_winrm_server_cert_validation: ignore
  - name: Ping the host
    win_ping:
    delegate_to: "{{hostName}}"
# Add a check to see if the instance is already in the database, just change the instanceid
  - name: Get the postifx
    set_fact:
      postfix: "{{host_name[7:] }}"
  - name: Show the postfix
    debug:
      var: postfix
  - name: Modify the user simetrical with the password for the user
    win_user:
      name: simetrical
      fullname: "{{username}}"
      description: "{{username}}"
      password: "Us3r{{postfix}}--"
      groups: 
        - Users
        - Remote Desktop Users
      state: present
    delegate_to: "{{hostName}}"
  - name: Rename the computer to USRSERVER{{postfix}}
    win_hostname:
      name: "USRSERVER{{postfix}}"
    delegate_to: "{{hostName}}"
#  - name: Check if the instance already exists
#    shell: mysql --batch --skip-column-names --port=5123 --database=app_produccion --host=database.simetrical.internal -e "select count(*) from instancias where subdominio = 'usuario{{usrserverid}}';"
#    register: subdomain
#  - name: Warn the user of an existing subdomain
#    debug:
#      msg: "Will remap the subdomain usuario{{usrserverid}} to instance {{instancedata.instances[0].instance_id}}"
#    when: subdomain.stdout != "0"
#  - name: Update the instance in the database
#    shell: mysql --port=5123 --database=app_produccion --host=database.simetrical.internal -e "update instancias set instancia='{{instancedata.instances[0].instance_id}}' where subdominio like 'usuario{{usrserverid}}';"
#    when: subdomain.stdout != "0"  
  - name: Create the instance in the database
    shell: mysql --port=5123 --database=app_produccion --host=database.simetrical.internal -e "insert into instancias(instancia,subdominio) values('{{instancedata.instances[0].instance_id}}', '{{host_name}}');"
  - name: Get the id for the last insert
    shell: mysql --port=5123 --database=app_produccion --host=database.simetrical.internal --skip-column-names -e "select id from instancias where instancia like '{{instancedata.instances[0].instance_id}}';"
    register: id_instancia
    ignore_errors: yes
  - debug: 
      var: id_instancia
#  - name: Check if the user is already registered
#    shell: mysql --batch --skip-column-names --port=5123 --database=app_produccion --host=database.simetrical.internal -e "select count(*) from usuarios where usuario like '{{ryver_username}}';"
#    register: userregistration
#  - name: Warn about the user already registered
#    debug:
#      msg: "User with ryver_username {{ryver_username}} is already registered. Not adding"
#    when: userregistration.stdout != "0"
  - name: Create the user and schedule in the database
    shell: mysql --port=5123 --database=app_produccion --host=database.simetrical.internal -e "insert into usuarios(usuario,horario) values('{{ryver_username}}', 'regular');"
  - name: Get the id for the last insert
    shell: mysql --port=5123 --database=app_produccion --host=database.simetrical.internal --skip-column-names -e "select id from usuarios where usuario like '{{ryver_username}}';"
    register: id_usuario
    ignore_errors: yes
  - debug: 
      var: id_usuario
  - name: Marry the user to the isntance
    shell: mysql --port=5123 --database=app_produccion --host=database.simetrical.internal -e "insert into usuarios_instancias(id_usuario,id_instancia,por_defecto) values('{{id_usuario.stdout}}', '{{id_instancia.stdout}}',1);"
# The file transferred to Windows, if edited in Linux should be converted to dos format
# via the unix2dos file
  - name: Copy the map_drives.bat to the remote host
    win_copy:
      src: map_drives_usrserver.bat
      dest: c:/temp/
    delegate_to: "{{hostName}}"
  - name: reboot the instance
    win_reboot:
    delegate_to: "{{hostName}}"
  - name: Inform the user of remaining steps
    debug:
      msg:
        - "There are several more steps you will need to do:"
        - "0. Create a conection in Guacamole for the Administrator of the new UsrServer and connect to the instance"
        - "1. Rename the simetrical user as usuario{{postfix}}"
        - "2. Allow newuser to shutdown the server gpedit.msc > Computer Configuration > Windows Settings > Security Settings > Local Policies > User Right Assignments > Shutdown the System > add newuser"
        - "3. Verify the user has copy/paste capability: gpedit.msc > Computer Configuration >  Administrative Templates > Windows Components > Terminal Services > Terminal Server > Device and Resource Redirection > Do not allow clipboard redirection > Disabled"
        - "4. Log as usuario{{postfix}}"
        - "5. Run c:\\temp\\map_drives_usrserver.bat"
        - "6. Create the svn user and assign permissions to them"
        - "6. Sync the repo"
        - "7. Inform the user of how to access the instance"
