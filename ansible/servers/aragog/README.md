# Setup of aragog.simetrical.net
## Introduction
aragog is the server for the simetrical.com site. Due to the fact that the sites was built by a third party, caution was taken to segregate it from our current network

## Creation of the instance

```bash
ansible-playbook -e "aws_creds=simetrical_creds.yaml new_instance=servers/aragog/aragog.yaml" --ask-vault-pass -v create_instance.yaml
```

## Installation of additional software 

```bash
ansible-playbook -e "extra_packages=servers/aragog/extra_packages.yaml" -v install_extra_packages.yaml
```

## Creation of users

```bash
for u in servers/aragog/users/*
do 
   ansible-playbook -e "user_vars=$u" manage_user.yaml
done
```

## mysql_secure_installation

Manual setup
