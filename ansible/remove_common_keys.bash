if [ $# -lt 2 ]
then
   echo Se esperaba un host sobre el cual operar y un usuario al cual removerle llaves
   echo $0 mfrserver.nissan ubuntu
   exit 1
fi

for u in eddie eddie-cel eddie-mac fran frank rafa
do
 echo Por remover la llave $u en el usuario $2 en la instancia $1  
 ansible-playbook --limit $1 --extra-vars "user=$2 oldkey=public_keys/$u.pub" remove_key_in_account.yaml
done
