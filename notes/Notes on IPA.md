# Notes on FreeIPA
- [Notes on FreeIPA](#notes-on-freeipa)
  - [Setup of a client](#setup-of-a-client)
    - [DNS](#dns)
    - [Ubuntu ipa-client package](#ubuntu-ipa-client-package)
      - [Installation log](#installation-log)
    - [Creation of home directories](#creation-of-home-directories)
      - [Ubuntu](#ubuntu)
      - [CentOS](#centos)
  - [References](#references)
## Setup of a client
### DNS
To add the search domain of "simetrical.internal", the file `/etc/systemd/resolved.conf` was edited. The following line was aded:
```
Domains=simetrical.internal
```
The `/etc/resolv.conf` file, after a reboot was similar to this
```
rvaldez@ipaclient:~$ cat /etc/resolv.conf
#
nameserver 127.0.0.53
options edns0 trust-ad
search simetrical.internal ec2.internal
```

### Ubuntu ipa-client package
The installed package was `freeipa-client`

#### Installation log
```
rvaldez@ipaclient:~$ sudo ipa-client-install
This program will set up FreeIPA client.
Version 4.8.6

WARNING: conflicting time&date synchronization service 'ntp' will be disabled in favor of chronyd

DNS discovery failed to determine your DNS domain
Provide the domain name of your IPA server (ex: example.com): simetrical.internal
Provide your IPA server name (ex: ipa.example.com): ipa.simetrical.internal
The failure to use DNS to find your IPA server indicates that your resolv.conf file is not properly configured.
Autodiscovery of servers for failover cannot work with this configuration.
If you proceed with the installation, services will be configured to always access the discovered server for all operations and will not fail over to other servers in case of failure.
Proceed with fixed values and no DNS discovery? [no]: yes
Do you want to configure chrony with NTP server or pool address? [no]:
Client hostname: ipaclient.simetrical.internal
Realm: SIMETRICAL.INTERNAL
DNS Domain: simetrical.internal
IPA Server: ipa.simetrical.internal
BaseDN: dc=simetrical,dc=internal

Continue to configure the system with these values? [no]: yes
Synchronizing time
No SRV records of NTP servers found and no NTP server or pool address was provided.
Using default chrony configuration.
Attempting to sync time with chronyc.
Time synchronization was successful.
User authorized to enroll computers: admin
Password for admin@SIMETRICAL.INTERNAL:
Successfully retrieved CA cert
    Subject:     CN=Certificate Authority,O=SIMETRICAL.INTERNAL
    Issuer:      CN=Certificate Authority,O=SIMETRICAL.INTERNAL
    Valid From:  2021-01-18 17:22:26
    Valid Until: 2041-01-18 17:22:26

Enrolled in IPA realm SIMETRICAL.INTERNAL
Created /etc/ipa/default.conf
Configured sudoers in /etc/nsswitch.conf
Configured /etc/sssd/sssd.conf
Configured /etc/krb5.conf for IPA realm SIMETRICAL.INTERNAL
Systemwide CA database updated.
Adding SSH public key from /etc/ssh/ssh_host_ecdsa_key.pub
Adding SSH public key from /etc/ssh/ssh_host_ed25519_key.pub
Adding SSH public key from /etc/ssh/ssh_host_dsa_key.pub
Adding SSH public key from /etc/ssh/ssh_host_rsa_key.pub
Could not update DNS SSHFP records.
SSSD enabled
Configured /etc/openldap/ldap.conf
Configured /etc/ssh/ssh_config
Configured /etc/ssh/sshd_config
Configuring simetrical.internal as NIS domain.
Client configuration complete.
The ipa-client-install command was successful
```
### Creation of home directories
#### Ubuntu
In Ubuntu, one of the references called for creation of `/usr/share/pam-configs/mkhomedir`, but one with the same name but similar contents was already present. However, enabling this module was still necessary, using 
```bash
$ sudo pam-auth-update
```

#### CentOS
In Centos, enabling the creation of home directories is done via
```
[root@ipa ~]# authconfig --enablemkhomedir --update
```


## References
- [Configure FreeIPA Client on Ubuntu 20.04|18.04 / CentOS 7](https://computingforgeeks.com/how-to-configure-freeipa-client-on-ubuntu-centos/): This reference was used to configure a client for the IPA domain
- [Chapter 5. Identity: Managing Users and User Groups](https://docs.fedoraproject.org/en-US/Fedora/18/html/FreeIPA_Guide/users.html#home-directories)