# Shell 

## Cambiar las extensiones de .yaml a .yml de los archivos en el directorio actual
```bash
for file in *yaml
do    
    filename="${file%.*}"
    mv $file $filename.yml
done
```
