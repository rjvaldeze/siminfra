#!/bin/bash
docker run -p 8080:8080 --name guacamole -detach --restart unless-stopped -v /simetrical/guacamole:/config  -v /var/log/guacamole:/usr/local/tomcat/logs oznu/guacamole
