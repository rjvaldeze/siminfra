#!/bin/bash
docker run -p 9090:8080 --name guacamole_openid --detach --restart unless-stopped -v /home/rvaldez/guacamole_openid:/config -e "EXTENSIONS=auth-openid"  oznu/guacamole
