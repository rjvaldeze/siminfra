#!/bin/bash
docker run -p 9090:8080 --name guacamole_ldap --detach --restart unless-stopped -v /home/rvaldez/guacamole_ldap:/config -e "EXTENSIONS=auth-ldap"  oznu/guacamole
